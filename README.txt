url dataframe utilizado: 
https://www.kaggle.com/abcsds/pokemon/data

PREGUNTAS

x Traducir los datos de ingles a español

x Grafico de torta con porcentajes por "Type 1"
x Grafico de torta con porcentajes por "Type 2"
x Grafico de barras con top 10 x=nombre y=stat(variable) filtrados por "Type 1"(variable) y 
x Grafico de barras con x="Type 1" y=cantidad filtrado por generacion
x Grafico de barras con x="type 2" y=cantidad filtrado por generacion
x Histograma: cantidad de pokemons para cada intervalo de stat(Variable)
Grafico de densidad: cantidad de pokemon por "type 1" por generacion

x Agrupados por type 1 (Variable) , cantidad de type 2 (Group By)
x Agrupados por generacion (variable), cantidad de pokemon sin type 2

x Para todos los legendarios, mostrar cantidad por tipo y generacion (CrossTab)
x Crosstab entre Type 1 y Type 2 (CrossTab)

x ¿Que pokemons tienen las mayores y menores stats para cada tipo de pokemon? max y min (Stat) por type 1
x ¿Que pokemons tienen las mayores y menores stats para cada generacion?max y min (stat) por generacion
x ¿Cual es el promedio de stats para cada tipo de pokemon? promedio de stat (Variable) por type 1
x ¿Cuel es el promedio de stats para los pokemons legendarios?
x ¿Cual es el promedio de stats para los pokemons no legendarios?
x ¿Cual es el promedio de stats total de todos los pokemon por generacion?

