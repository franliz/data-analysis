# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# Cargando datos
import pandas as pd
pkmn = pd.read_csv('Pokemon.csv')

# Para revisar los datos cargados ejecutar la siguiente línea de código
# print(pkmn)

#LEGIBILIDAD

# Traduccion de los datos al español
# Para realizar este cambio, primero se ejecutó la siguiente linea de codigo:
# type1 = pkmn['Type 1'].sort_values().unique() 
# con el fin de obtener todos los tipos (tipo 1) de pokemon que existian en el dataframe

# Cambiando los datos de Type 1
pkmn['Type 1'] = pkmn['Type 1'].map({'Grass': 'Planta', 'Fire': 'Fuego',
                                    'Water': 'Agua', 'Bug': 'Bicho',
                                    'Normal': 'Normal', 'Poison': 'Veneno',
                                    'Electric': 'Electrico', 'Ground': 'Tierra',
                                    'Fairy': 'Hada', 'Fighting': 'Lucha',
                                    'Psychic': 'Psiquico', 'Rock': 'Roca',
                                    'Ghost': 'Fantasma', 'Ice': 'Hielo',
                                    'Dragon': 'Dragon', 'Dark': 'Siniestro',
                                    'Steel': 'Acero', 'Flying': 'Volador'})

# Cambiando los datos de Type 2
pkmn['Type 2'] = pkmn['Type 2'].map({'Grass': 'Planta', 'Fire': 'Fuego',
                                    'Water': 'Agua', 'Bug': 'Bicho',
                                    'Normal': 'Normal', 'Poison': 'Veneno',
                                    'Electric': 'Electrico', 'Ground': 'Tierra',
                                    'Fairy': 'Hada', 'Fighting': 'Lucha',
                                    'Psychic': 'Psiquico', 'Rock': 'Roca',
                                    'Ghost': 'Fantasma', 'Ice': 'Hielo',
                                    'Dragon': 'Dragon', 'Dark': 'Siniestro',
                                    'Steel': 'Acero', 'Flying': 'Volador'})
# Reemplazando los datos NaN por Ninguno (para Type 2)
pkmn['Type 2'] = pkmn['Type 2'].fillna('Ninguno')

# Cambiando los datos de Legendary
pkmn['Legendary'] = pkmn['Legendary'].map({True: 'Si', 
                                           False: 'No'})
#GRÁFICOS
import matplotlib.pyplot as plt

#Porcentaje de Pokemons por tipo 1
type1 = pkmn['Type 1'].sort_values().unique()
figura = plt.figure(figsize=(20,10))
data1 = pkmn['Type 1'].value_counts().sort_index()
plt.pie(data1, labels=type1 ,autopct='%1.1f%%')
plt.title("Porcentaje de Pokemons por Tipo 1")
plt.show()

import matplotlib.pyplot as plt

#Porcentaje de Pokemons por tipo 2
type2 = pkmn['Type 2'].sort_values().unique()
figura = plt.figure(figsize=(20,10))
data2 = pkmn['Type 2'].value_counts().sort_index()
plt.pie(data2, labels=type2, autopct='%1.1f%%')
plt.title("Porcentaje de Pokemons por Tipo 2")
plt.show()

# Grafico de barras con top 10 (más alto) según stat (variable)
stat = 'HP' #Poner el stat que se desea visualizar en el grafico
data3 = pkmn[['Name', stat]].nlargest(10, stat)
data3.plot.bar(x='Name', figsize=(15,7.5))
titulo = "TOP 10 DE LOS POKEMONS CON MÁS " + stat
plt.title(titulo)
plt.show()

# Grafico de barras que muestra la cantidad de Pokemons por tipo 1, filtrado por generacion (variable)
gen = 1 #Generaciones de 1 a 6
filtro4 = pkmn.Generation==gen
data4 = pkmn[filtro4]['Type 1'].value_counts()
data4.plot.bar(x='Type 1', figsize=(15,7.5))
titulo2 = "CANTIDAD DE POKEMONS POR TIPO 1 PARA LA GENERACIÓN " + str(gen)
plt.title(titulo2)
plt.show()

# Grafico de barras que muestra la cantidad de Pokemons por tipo 2, filtrado por generacion (variable)
gen = 2 #Generaciones de 1 a 6
filtro5 = pkmn.Generation==gen
data5 = pkmn[filtro5]['Type 2'].value_counts()
data5.plot.bar(x='Type 2', figsize=(15,7.5))
titulo3 = "CANTIDAD DE POKEMONS POR TIPO 2 PARA LA GENERACIÓN " + str(gen)
plt.title(titulo3)
plt.show()

# Histograma que presenta la cantidad de pokemons que poseen cierto rango de stat (variable)
stat = 'Attack' #escriba acá el stat que desea mostrar
pkmn.hist(column=stat)

# Grafico de densidad
"""
import seaborn as sns
for x in type1:
    subdata = pkmn[pkmn['Type 1']==x]
    sns.distplot(subdata[stat], hist=False, kde=True, kde_kws={'linewidth': 3})

##pkmn['Attack'].plot.kde()
##plt.show()
"""

# Cantidad de Pokemons por cada Type 2 agrupados por Type 1 (podemos revisar, por ejemplo, cantidad de Pokemons Acero (Tipo 1) cuyo Tipo 2 es Dragon)
print("--------------------------------------------------")
print("POKEMONS AGRUPADOS POR TIPO 1 Y TIPO 2")
print("--------------------------------------------------")
xx = pkmn.groupby(['Type 1', 'Type 2'])['Type 2'].count()
print(xx)
print("--------------------------------------------------")

# Cantidad de pokemons sin Tipo 2 para cada generación
print("POKEMONS SIN TIPO 2 POR GENERACION")
print("--------------------------------------------------")
filtroxy = pkmn['Type 2']=='Ninguno'
xy = pkmn[filtroxy].groupby(['Generation', 'Type 2'])['Type 2'].count()
print(xy)
print("--------------------------------------------------")

# Cantidad de pokemons legendarios según generación y tipo 1
print("POKEMONS LEGENDARIOS POR GENERACION Y TIPO 1")
print("--------------------------------------------------")
filtroLeg = pkmn.Legendary=='Si'
legendarios = pkmn[filtroLeg]
ct2 = pd.crosstab(legendarios['Type 1'], legendarios['Generation'])
print(ct2)
print("--------------------------------------------------")

# Tabla cruzada que muestra la cantidad de pokemons de Tipo 1 que además son Tipo 2
print("TABLA CRUZADA ENTRE TIPO 1 Y TIPO 2")
print("--------------------------------------------------")
asd = pd.crosstab(pkmn['Type 1'], pkmn['Type 2'])
print(asd)
print("--------------------------------------------------")

# Encontrar el pokemon (nombre) que pertenezca a tipo 1 (variable) con más stat (variable)
tipo = 'Planta'
atributo = 'HP'
print("POKEMON CON MÁS ", atributo, "PARA EL TIPO 1: ", tipo)
print("--------------------------------------------------")
tipopkmn = pkmn['Type 1'] == tipo
pmyTipo = pkmn[tipopkmn][['Name', atributo]].nlargest(1, atributo)
print(pmyTipo)
print("--------------------------------------------------")

# Encontrar el pokemon (nombre) que pertenezca a tipo 1 (variable) con menos stat (variable)
print("POKEMON CON MENOS ", atributo, "PARA EL TIPO 1: ", tipo)
print("--------------------------------------------------")
pmnTipo = pkmn[tipopkmn][['Name', atributo]].nsmallest(1, atributo)
print(pmnTipo)
print("--------------------------------------------------")

# Encontrar el pokemon (nombre) que pertenezca a una generación (variable) con más stat (variable)
gen = 2
atributo = 'HP'
print("POKEMON DE LA GENERACION: ", gen, " CON MÁS ", atributo)
print("--------------------------------------------------")
tipopkmn = pkmn['Generation'] == gen
pmyGen = pkmn[tipopkmn][['Name', atributo]].nlargest(1, atributo)
print(pmyGen)
print("--------------------------------------------------")

# Encontrar el pokemon (nombre) que pertenezca a una generación (variable) con menos stat (variable)
print("POKEMON DE LA GENERACION: ", gen, " CON MENOS ", atributo)
print("--------------------------------------------------")
pmnGen = pkmn[tipopkmn][['Name', atributo]].nsmallest(1, atributo)
print(pmnGen)
print("--------------------------------------------------")

# Lista de los mayores stats agrupados por Tipo 1
print("MAYORES STATS POR TIPO 1")
print("--------------------------------------------------")
maxStats = pkmn.groupby('Type 1')['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed'].max()
print(maxStats)
print("--------------------------------------------------")
# Lista de los menores stats agrupados por Tipo 1
print("MENORES STATS POR TIPO 1")
print("--------------------------------------------------")
minStats = pkmn.groupby('Type 1')['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed'].min()
print(minStats)
print("--------------------------------------------------")
# Lista del promedio de stats agrupados por Tipo 1
print("PROMEDIO DE STATS POR TIPO 1")
print("--------------------------------------------------")
meanStats = pkmn.groupby('Type 1')['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed'].mean().round()
print(meanStats)
print("--------------------------------------------------")
# Lista del promedio de stats de todos los pokemons que son legendarios
print("PROMEDIO DE STATS PARA LOS POKEMONS LEGENDARIOS")
print("--------------------------------------------------")
meanLeg = legendarios[['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed']].mean().round()
print(meanLeg)
print("--------------------------------------------------")
# Lista del promedio de stats para todos los pokemons que no son legendarios
print("PROMEDIO DE STATS PARA LOS POKEMONS NO LEGENDARIOS")
print("--------------------------------------------------")
filtroLegNo = pkmn.Legendary=='No'
legendariosNo = pkmn[filtroLegNo]
meanNoLeg = legendariosNo[['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed']].mean().round()
print(meanNoLeg)
print("--------------------------------------------------")
# Lista del promedio de stats por generación
print("PROMEDIO DE STATS PARA CADA UNA DE LAS GENERACIONES")
print("--------------------------------------------------")
meanGen = pkmn.groupby('Generation')['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed'].mean().round()
print(meanGen)
print("--------------------------------------------------")
# Creando un gráfico de densidad para un atributo (variable) por generación
for gen in pkmn.Generation.unique():
    stat = 'Total'
    filtro = pkmn.Generation==gen
    etiqueta = "Generación " + str(gen)
    data = pkmn[filtro]
    data[stat].plot.kde(label=etiqueta, legend=True)





